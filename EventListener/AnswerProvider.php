<?php

namespace Tvt\AnswerBundle\EventListener;

use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\GenericEvent;

final class AnswerProvider
{
    const MESSAGE_FAIL = 'Nothing is here, sorry';
    const MESSAGE_SUCCESS = 'One of our passwords is... "%s"';

    public function onRequestAnswer(GenericEvent $event) {
        $output = $event->getArgument('symfonyStyle');

        if(!$output instanceof SymfonyStyle) {
            throw new \InvalidArgumentException('Missing argument symfonyStyle');
        }

        $isVisible = $event->getArgument('isVisible');

        if(!$isVisible) {
             $this->printFail($output);
             return;
        }

        $this->printSuccess($output);
    }

    private function printFail(SymfonyStyle $output)
    {
        $output->note(self::MESSAGE_FAIL);
    }

    private function printSuccess(SymfonyStyle $output)
    {
        $output->success(sprintf(self::MESSAGE_SUCCESS, "It's not a bug, it's a feature!"));
    }
}